# Prérequis
La construction de l'image nécessite un compte actif sur [edocs](http://edocs.wedia.fr).

# Procédure de création de l'image
1. Clonez ce projet
2. Allez dans votre clone
3. Tapez `docker build --build-arg EDOCS_LOGIN=<login edocs> --build-arg EDOCS_PASS=<password edocs> -t wedia .`

Les paramètres `EDOCS_LOGIN` et `EDOCS_PASS` sont obligatoires. Sans eux, la construction de l'image échouera.  

La construction de l'image s'appuit par défaut sur la dernière version disponible des binaires Wedia ( 11.4.3 au moment de l'écriture
de cette doc ). Il est toutefois possible de s'appuyer sur une version alternative en précisant la version avec l'argument `WEDIA_VERSION`

Exemple :
`docker build --build-arg EDOCS_LOGIN=foo --build-arg EDOCS_PASS=pass --build-arg WEDIA_VERSION=11.4.1 -t wedia .` 

# Créer le container
Tapez simplement la ligne suivante :
`docker run -itp 8080:8080 --name <Nom du container> wedia`

# Travailler avec le container 
La ligne précédente instancie un container à partir de l'image crée. Chaque appel crée un nouveau container. 
Pour travailler sur un container existant. Executez la ligne commande suivante :
`docker start -i <ID ou NOM de votre container>`
Vous pouvez retrouver l'id en tapant `docker ps -a`

# Infos utiles pour configurer wedia
Tomcat est exécuté avec le compte wedia. Les répertoires SAN et SANADMIN doivent donc être créés dans son répertoire $HOME :
`/home/wedia/`

Un compte MySQL a été créé avec tous les droits sur toutes les bases : login/pass = wedia/wedia

Une base `wedia` a été créée. 