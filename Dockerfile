FROM ubuntu:16.04
MAINTAINER Cédric ALCISIADI <cedric.alcisiadi@wedia-group.com>

# Arguments indispensable pour récupérer les binaires Wedia
ARG WEDIA_VERSION=11.4.3

RUN { \
    echo mysql-server-5.7 mysql-server/root_password password root; \
    echo mysql-server-5.7 mysql-server/root_password_again password root; \
  } | debconf-set-selections \
  && apt-get update \
  && apt-get install -y --no-install-recommends \
    tomcat7-user \
    mysql-server-5.7 \
    imagemagick \
    libav-tools \
    phantomjs \
    elasticsearch \
    curl \
    unzip 

# Ajout de la ligne lower_case_table_names dans /etc/mysl/my.cnf
# On force en MyISAM car InnoDB sur Mysql 5.7 est très lent
RUN sed -e '/\[mysqld\]/a lower_case_table_names=1\ninnodb_flush_log_at_trx_commit=0\ndefault-storage-engine=MyISAM' -i /etc/mysql/mysql.conf.d/mysqld.cnf

# Création de la base de données mysql
RUN service mysql start \
  && mysqladmin -u root -proot create wedia \
  && echo "grant all privileges on *.* to 'wedia'@'%' identified by 'wedia'; flush privileges;" | mysql -u root -proot \
  && service mysql stop

# Virer les delegates d'imagemagick
#sed -i.bak '/pattern to match/d' /etc/imagemagick/delegates.xml

# Création de notre utilisateur
RUN useradd -mr wedia

# On ajoute le driver mysql aux libs de Tomcat
COPY mysql-connector-java-5.1.39-bin.jar "/usr/share/tomcat7/lib/"

# Toutes les commandes à partir de maintenant s'éxécutent dans le contexte de notre utilisateur
USER wedia

# Création de notre tomcat privé
RUN /usr/bin/tomcat7-instance-create "$HOME/apps/tomcat" 

# Le template par défaut d'une appli Tomcat est TRES avare en mémoire
RUN sed -i.deb -E "s/Xmx[^\\s\"]+/Xmx2G/" "$HOME/apps/tomcat/bin/setenv.sh"

# Configuration des users et rôle de tomcat 
RUN sed -e '/<tomcat-users>/a <role rolename="Administrators"/><user username="admin" password="admin" roles="Administrators"/>' \
  -i $HOME/apps/tomcat/conf/tomcat-users.xml

# Récupération des binaires Wedia : demande les infos d'authentification EDOCS 
ARG EDOCS_LOGIN
ARG EDOCS_PASS

RUN test -n "${EDOCS_LOGIN}" && test -n "${EDOCS_PASS}" \
  && curl -u "${EDOCS_LOGIN}:${EDOCS_PASS}" "http://edocs.wedia.fr/dav/san/_distributions/engine/${WEDIA_VERSION}/full/tomcat%206.x.zip" -o /tmp/wedia.zip \
  && curl -u "${EDOCS_LOGIN}:${EDOCS_PASS}" "http://edocs.wedia.fr/dav/san/_distributions/engine/${WEDIA_VERSION}/full/ImagingServer.zip" -o /tmp/imagingserver.zip

# Déploiement de la webapp
RUN unzip -j /tmp/wedia.zip webapp/noheto.war -d "$HOME/apps/tomcat/webapps/"
RUN mv "$HOME/apps/tomcat/webapps/noheto.war" "$HOME/apps/tomcat/webapps/ROOT.war"
RUN rm -f /tmp/wedia.zip

# Suppression de la webapp par défaut 
RUN rm -Rf "$HOME/apps/tomcat/webapps/ROOT"

# Déploiement d'ImagingServer
RUN mkdir -p "$HOME/apps/imagingserver" \
  && unzip /tmp/imagingserver.zip -d "$HOME/apps/imagingserver"
RUN chmod og+x "$HOME/apps/imagingserver/imagingserver.sh"
RUN rm -f /tmp/imagingserver.zip

# Ouverture des port de Tomcat
EXPOSE 8080
EXPOSE 8443

USER root

# Copie du script de démarrage
COPY wedia.sh /usr/local/sbin/
RUN chmod o+x /usr/local/sbin/wedia.sh

# Le lancement de l'image démarre Mysql, ElasticSearch, ImagingServer pour finir par Tomcat
CMD ["-c", "/usr/local/sbin/wedia.sh"]
ENTRYPOINT ["/bin/sh"]
