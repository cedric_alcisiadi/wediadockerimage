#!/bin/sh

service mysql start 

service elasticsearch start 

# Démarrage d'ImagingServer
su -s "/bin/sh" wedia -c "/home/wedia/apps/imagingserver/imagingserver" &

# Démarrage de tomcat
su -s "/bin/sh" wedia -c "CATALINA_BASE=/home/wedia/apps/tomcat /usr/share/tomcat7/bin/catalina.sh run"

service mysql stop 

service elasticsearch stop 
